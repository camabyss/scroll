UpdateSizes = {}
attributes = ['href','src','id','alt']
function Scroll (parent) {
	this.parent = parent
	this.eDict = {}
	this.Dragging = false
	this.hor = false
	this.ver = false
	this.size = 10
	this.Build = function () {
		this.parent.css('overflow','hidden')
		srcollW = Scroll_barWidth ()
		Scroll_ME(this.eDict,'scroll',this.parent,
			{'width':'100%+'+srcollW,
			'height':'100%+'+srcollW,
			'overflow':'scroll',
			'float':'left'}
		)
		Scroll_ME(this.eDict,'content',this.eDict['scroll'],
			{
			'float':'left'}
		)
		Scroll_ME(this.eDict,'trough',this.parent,
			{'width':0,
			'height':'100%',
			'background-color':'rgba(0,0,0,0)',
			'position':'relative',
			'opacity':0,
			top:0,
			left:this.parent.width()}
		)
		Scroll_ME(this.eDict,'bar',this.eDict['trough'],
			{'width':this.size,
			'border-radius':this.size/2,
			'background-color':'#fff',
			'box-shadow':'inset -1px -1px 2px #888'}
		)
		Scroll_ME(this.eDict,'h trough',this.parent,
			{'height':0,
			'width':'100%',
			'background-color':'rgba(0,0,0,0)',
			'position':'relative',
			'opacity':0,
			top:0,
			left:0}
		)
		Scroll_ME(this.eDict,'h bar',this.eDict['h trough'],
			{'height':this.size,
			'border-radius':this.size/2,
			'background-color':'#fff',
			'box-shadow':'inset -1px -1px 2px #888'}
		)
		this.Apply(this)
	}
	this.Add = function () {
		return this.eDict['content']
	}
	this.Apply = function (This) {
		Scroll_hoverIntent(This.parent,[[This.eDict['trough'],{opacity:1},{opacity:0}],[This.eDict['h trough'],{opacity:1},{opacity:0}]])
		This.eDict['bar'].draggable({ axis:"y",containment:'parent',
			start:function () {
				This.Dragging = true
			},
			stop:function () {
				This.Dragging = false
			},
			drag:function () {
				This.eDict['scroll'][0].scrollTop =
					(This.eDict['bar'].offset().top-
					 This.eDict['scroll'].offset().top)/
						(This.eDict['trough'].height()/
						 This.eDict['content'].height())
		}})
		This.eDict['h bar'].draggable({ axis:"x",containment:'parent',
			start:function () {
				This.Dragging = true
			},
			stop:function () {
				This.Dragging = false
			},
			drag:function () {
				This.eDict['scroll'][0].scrollLeft = 
					(This.eDict['h bar'].offset().left-
					This.eDict['scroll'].offset().left)/
						(This.eDict['h trough'].width()/
						 This.eDict['content'].width())
		}})
		This.eDict['scroll'].scroll(
			function (){
				if (This.Dragging == false) {
					if (This.hor) {
						This.eDict['h bar'].width(
							(This.eDict['h trough'].width()*(
								This.eDict['scroll'].width()/
								This.eDict['content'].width()
							))-10)
						This.eDict['h bar'].css('left',
							This.eDict['scroll'][0].scrollLeft*(
								This.eDict['h trough'].width()/
								This.eDict['content'].width()
							))
					}
					if (This.ver) {
						This.eDict['bar'].height(
							(This.eDict['trough'].height()*(
								This.eDict['scroll'].height()/
								This.eDict['content'].height()
							))-10)
						This.eDict['bar'].css('top',
							This.eDict['scroll'][0].scrollTop*(
								This.eDict['trough'].height()/
								This.eDict['content'].height()
							))
					}
				}
		})
		This.eDict['content'].bind('contentchange', function() {
			This.Update()
		});
	}
	this.Update = function () {
		Scroll_DoUpdateSizes(this.eDict['scroll'])
		if (this.eDict['content'].height() <= this.parent.height()) {
			this.ver = false
			this.eDict['trough'].css({left:this.parent.width()})
		} else {
			this.ver = true
			this.eDict['trough'].css({left:this.parent.width()-this.size})
		}
		if (this.eDict['content'].width() <= this.parent.width()) {
			this.hor = false
			this.eDict['h trough'].css({top:0})
		} else {
			this.hor = true
			this.eDict['h trough'].css({top:-this.size})
		}
		this.eDict['scroll'].scroll()
	}
	this.Build()
}
function Scroll_s (peram,value,element) {
    if (typeof peram == typeof {} && value == undefined) {
	value = peram.value
	element = peram.element
	peram = peram.peram
    }
    //if array:
    if (typeof value == typeof {}) {
	//calculate new setting
	temp = 1
	for (a in value){
	    temp*=value[a]
	}
    //else if string:
    } else if (typeof value == typeof '' &&
		(value.indexOf('%')!=-1 &&
		    (value.indexOf('+')!=-1 ||
		    value.indexOf('-')!=-1
		    )
		)) {
	//split into level,%,op and px
	level = value.lastIndexOf('<')+1
	percent = parseFloat(value.slice(level,value.indexOf('%')))
	oper = value.slice(value.indexOf('%')+1,value.indexOf('%')+2)
	px = parseFloat(value.slice(value.indexOf(oper)+1))
	//alert(name+','+level+','+percent+','+oper+','+px)
	pobj = element.parent()
	for (var i=0;i<level;i++) {
		pobj = pobj.parent()
	}
	pobjs = parseFloat(pobj.css(peram).slice(0,-2))
	//calculate new setting
	if (oper == '-') {
		temp = (pobjs*(percent/100))-px
	} else if (oper == '+') {
		temp = (pobjs*(percent/100))+px
	}else if (oper == '*') {
		temp = (pobjs*(percent/100))*px
	}
    } else {
	temp = value
    }
    return temp
}
function Scroll_barWidth () {  
    var inner = document.createElement('p');  
    inner.style.width = "100%";  
    inner.style.height = "200px";  
  
    var outer = document.createElement('div');  
    outer.style.position = "absolute";  
    outer.style.top = "0px";  
    outer.style.left = "0px";  
    outer.style.visibility = "hidden";  
    outer.style.width = "200px";  
    outer.style.height = "150px";  
    outer.style.overflow = "hidden";  
    outer.appendChild (inner);  
  
    document.body.appendChild (outer);  
    var w1 = inner.offsetWidth;  
    outer.style.overflow = 'scroll';  
    var w2 = inner.offsetWidth;  
    if (w1 == w2) w2 = outer.clientWidth;  
  
    document.body.removeChild (outer);  
  
    return (w1 - w2);  
};
function Scroll_TypeOf (object) {
    if (typeof object == 'object') {
	if (object instanceof Array) {
	    return 'array'
	} else {
	    return 'dict'
	}
    } else {
	return typeof object
    }
}
(function(){
        var interval;
        jQuery.event.special.contentchange = {
        setup: function(){
            var self = this,
            $this = $(this),
            $originalContent = $this.html();
            interval = setInterval(function(){
                if($originalContent != $this.html()) {
                        $originalContent = $this.html();
                    jQuery.event.handle.call(self, {type:'contentchange'});
                }
            },100);
        },
        teardown: function(){
            clearInterval(interval);
        }
    };

})();
function Scroll_ME (eDict,name,parent) {
    //Defaults and options
    tag = 'div'
    classes = []
    settings = {}
    buildNow = true
    updates = false
    for (var a=3;a<arguments.length;a++){
        switch(Scroll_TypeOf(arguments[a])) {
            case 'string':
            tag = arguments[a]
            break;
            case 'array':
            classes = arguments[a]
            break;
            case 'dict':
            settings = arguments[a]
            break;
            case 'boolean':
            buildNow = arguments[a]
            break;
        }
    }
    if (eDict != false && eDict != undefined && eDict != null) {
        obj = eDict[name] =
            $('<'+tag+'/>',
                {
                    'class':name+' '+classes.join(' ')
                }
            )
    } else {
        obj =
            $('<'+tag+'/>',
                {
                    'class':name+' '+classes.join(' ')
                }
            )
    }
    //Apply settings
    updateSettings = {}
    for (s in settings) {
        if (typeof settings[s] == typeof [] ||
            (typeof settings[s] == typeof '' &&
            (settings[s].indexOf('%')!=-1 &&
                (settings[s].indexOf('+')!=-1 ||
                settings[s].indexOf('-')!=-1
                )
            )
            )
        ){
            updateSettings[s] = settings[s]
            updates = true
        }
        else if (attributes.indexOf(s) != -1) {
            obj.attr(s,settings[s])
        }
        else if (s == 'width') {
            obj.width(settings[s])
        }
        else if (s == 'height') {
            obj.height(settings[s])
        }
        else {
            obj.css(s,settings[s])
        }
    }
    if (updates == true) {
        Uid = name
        UN = 0
        while (Uid in UpdateSizes) {
            Uid = name+UN
            UN++
        }
        obj.Uid = Uid
        UpdateSizes[Uid] = {'obj':obj,'set':updateSettings}
    }
    if (buildNow != false) {
        parent.append(obj)
    }
    return obj
}
function Scroll_DoUpdateSizes (element) {
    if (element != undefined && element.Uid != undefined) {
	Uid = element.Uid
	    for (s in UpdateSizes[Uid].set) {
		temp = Scroll_s (
		    s,
		    UpdateSizes[Uid].set[s],
		    UpdateSizes[Uid].obj
		)
		UpdateSizes[Uid].obj.css(s,temp)
	    }
    } else {
	done = false
	runs = 0
	while (!done && runs<10) {
	    runs++
	    all = true
	    for (Uid in UpdateSizes) {
		for (s in UpdateSizes[Uid].set) {
		    temp = Scroll_s (
			s,
			UpdateSizes[Uid].set[s],
			UpdateSizes[Uid].obj
		    )
		    otemp = parseFloat(UpdateSizes[Uid].obj.css(s).slice(0,-2))
		    if (otemp != temp) {
			UpdateSizes[Uid].obj.css(s,temp)
			all = false
		    }
		}
	    }
	    if (all) {
		done = true
	    }
	}
    }
}
function Scroll_hoverIntent(hoverElement,delta,duration) {
	if (delta != 're' && delta != null){
		//hover_log[hoverElement] = [delta,duration]
		hoverElement.hoverIntent(
			function(){
				for (d in delta) {
					delta[d][0].stop();
					if (delta[d][1].length != undefined) {
						for (D in delta[d][1]) {
							if (delta[d][1][D] == 'clear') {
								delta[d][0].clearQueue()
							} else if (typeof delta[d][1][D] == typeof{}){
							    if (duration != false && delta[d][3] != false) {
								delta[d][0].animate(delta[d][1][D],duration);
							    } else {
								delta[d][0].css(delta[d][1][D]);
							    }
							} else if (typeof delta[d][1][D] == typeof 1) {
								delta[d][0].delay(delta[d][1][D]);
							}
						}
					} else {
					    if (duration != false && delta[d][3] != false) {
						delta[d][0].animate(delta[d][1],duration);
					    } else {
						delta[d][0].css(delta[d][1]);
					    }
					}
				}				
			},function(){
				for (d in delta) {
					delta[d][0].stop()
					if (delta[d][2].length != undefined) {
						for (D in delta[d][2]) {
							if (delta[d][2][D] == 'clear') {
								delta[d][0].clearQueue()
							} else if (typeof delta[d][2][D] == typeof{}){
							    if (duration != false && delta[d][3] != false) {
								delta[d][0].animate(delta[d][2][D],duration);
							    } else {
								delta[d][0].css(delta[d][2][D]);
							    }
							} else if (typeof delta[d][2][D] == typeof 1) {
								delta[d][0].delay(delta[d][2][D]);
							}
						}
					} else {
					    if (duration != false && delta[d][3] != false) {
						delta[d][0].animate(delta[d][2],duration);
					    } else {
						delta[d][0].css(delta[d][2]);
					    }
					}
				}
			}
		)
	} else if (delta == null) {
		hoverElement.hoverIntent(function(){},function(){})
	} else if (delta == 're'){
		//alert(hover_log[hoverElement][0])
		Scroll_hoverIntent(hoverElement,hover_log[hoverElement][0],hover_log[hoverElement][1])
	}
}